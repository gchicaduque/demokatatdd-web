function redirect(page) {
    location.href = page;
}

function inicializarDataTable(id, numColExclude) {
    $(id).dataTable({
        "oLanguage": {
            "sSearch": "Filtrar:",
            "sEmptyTable": "No hay información para mostrar",
            "sInfo": "Mostrando del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty": "No hay información para mostrar",
            "sInfoFiltered": "(Filtrado de _MAX_ entradas)",
            "sLengthMenu": "_MENU_ entradas por página",
            "oPaginate": {
                "sFirst": "First page",
                "sLast": "Primera Página",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        "aoColumnDefs": [
            {'bSortable': false, 'aTargets': numColExclude}
        ]
    });
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};