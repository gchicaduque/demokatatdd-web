from common import *

# DATABASE CONFIGURATION
import dj_database_url

DATABASES = {
    'default': dj_database_url.config()
}
# END DATABASE CONFIGURATION

# ALLOWED HOSTS CONFIGURATION
# Set the heroku host for Quality Assurance
ALLOWED_HOSTS = ['*']
# END ALLOWED HOST CONFIGURATION
