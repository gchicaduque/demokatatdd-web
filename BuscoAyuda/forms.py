# coding=utf-8
from django import forms
from django.forms import ModelForm

from BuscoAyuda.models import *


class IndependienteForm(ModelForm):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput())
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)
    experiencia = forms.IntegerField()
    servicio = forms.Select()
    telefono = forms.IntegerField()
    email = forms.EmailField(max_length=254)
    foto = forms.ImageField()

    class Meta:
        model = Independiente
        fields = [
            'username',
            'password',
            'first_name',
            'last_name',
            'experiencia',
            'servicio',
            'telefono',
            'email',
            'foto'
        ]

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username='username'):
            raise forms.ValidationError('Nombre de usuario ya registrado.')
        return username


class IndependienteForm2(ModelForm):
    class Meta:
        model = Independiente
        exclude = ['usuario']

    def save(self, commit=True):
        super(IndependienteForm2, self).save(commit=commit)

    def __init__(self, *args, **kwargs):
        super(IndependienteForm2, self).__init__(*args, **kwargs)
        self.fields['experiencia'].widget.attrs \
            .update({
            'placeholder': 'Experiencia del Independiente',
            'class': 'form-control'
        })
        self.fields['telefono'].widget.attrs \
            .update({
            'placeholder': 'Teléfono del Independiente',
            'class': 'form-control'
        })
        self.fields['servicio'].widget.attrs \
            .update({
            'placeholder': 'Servicio del Independiente',
            'class': 'form-control'
        })


class ComentarioForm(ModelForm):
    contenido = forms.CharField(widget=forms.Textarea)
    independiente_comentario = forms.CharField(widget=forms.HiddenInput(), required=True)

    class Meta:
        model = Comentario
        exclude = ['independiente', 'fecha']

    def save(self, commit=True):
        super(ComentarioForm, self).save(commit=commit)

    def __init__(self, *args, **kwargs):
        super(ComentarioForm, self).__init__(*args, **kwargs)
        self.fields['contenido'].widget.attrs \
            .update({
            'placeholder': 'Contenido del Comentario',
            'class': 'form-control'
        })
        self.fields['correo'].widget.attrs \
            .update({
            'placeholder': 'Correo',
            'class': 'form-control'
        })


class UserForm(ModelForm):
    class Meta:
        model = User
        exclude = [
            'password',
            'last_login',
            'is_superuser',
            'groups',
            'user_permissions',
            'is_staff',
            'is_active',
            'date_joined',
            'is_staff',
        ]

    def save(self, commit=True):
        super(UserForm, self).save(commit=commit)

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs \
            .update({
            'placeholder': 'Usuario del Independiente',
            'class': 'form-control'
        })
        self.fields['first_name'].widget.attrs \
            .update({
            'placeholder': 'Nombres del Independiente',
            'class': 'form-control'
        })
        self.fields['last_name'].widget.attrs \
            .update({
            'placeholder': 'Apellidos del Independiente',
            'class': 'form-control'
        })
        self.fields['email'].widget.attrs \
            .update({
            'placeholder': 'Email del Independiente',
            'class': 'form-control'
        })
