from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models


class Servicio(models.Model):
    nombre = models.CharField(max_length=200)

    def __unicode__(self):
        return u'%s' % self.nombre


class Independiente(models.Model):
    experiencia = models.IntegerField()
    telefono = models.BigIntegerField()
    foto = models.ImageField(upload_to='fotos')
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    servicio = models.ForeignKey(Servicio)

    def __unicode__(self):
        return u'%s' % self.usuario.username


class Comentario(models.Model):
    contenido = models.CharField(max_length=256)
    correo = models.EmailField()
    fecha = models.DateField(auto_now_add=True)
    independiente = models.ForeignKey(Independiente)

    def __unicode__(self):
        return u'%s' % self.contenido
