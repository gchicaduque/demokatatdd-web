from django import template

register = template.Library()


@register.filter
def range(value):
    return xrange(value)


@register.simple_tag
def active(request, pattern):
    if request.path == pattern:
        return 'active'
    return ''


@register.simple_tag
def activecontains(request, pattern):
    if pattern in request.path:
        return 'active'
    return ''
